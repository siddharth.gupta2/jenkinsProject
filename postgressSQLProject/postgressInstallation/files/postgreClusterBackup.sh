#!/bin/bash --
#-----------------------------------------------------
# @Date: Tue Mar 30 15:13:12 IST 2021
# @Author: SIDDHARTH GUPTA
# @Batch: Ninja Batch 9
# @Group: NinjaWarriors
#-----------------------------------------------------
# Problem Statement:
# To create a cpmplete cluster back up
#
# Execution: 
#  ./scripname
#  ./postgreDbBackup.sh 
#
# Output Backup File Location:
# sample output file
# /tmp/Postgress/BackUp_postgres-20210330

clusterName=postgres_cluster
clusterDir=Cluster_Backup
date_parameter=$(date '+%Y%m%d')
name=$clusterName-$date_parameter

createClusterDump(){
    pg_dumpall | gzip > /tmp/Postgress/BackUp/Cluster_Backup/$name-backup.gz
    echo "Backup Location: /tmp/Postgress/BackUp/$clusterDir"
}

start(){
        echo "BackUp: $clusterName"
        cd /tmp/Postgress/BackUp
        check1=`ls -l Cluster_Backup> /dev/null 2>&1`
        validation1=`echo $?`
        #echo $validation1
        if [ $validation1 == '0' ];
        then
            createClusterDump
        else 
            mkdir $clusterDir
            createClusterDump
        fi
}

#The starting point of the code
start
