#!/bin/bash --
su -c ~/postgresClusterSetup.sh root
#-----------------------------------------------------
# @Date: Mon Mar 29 08:51:49 IST 2021
# @Author: SIDDHARTH GUPTA
# @Batch: Ninja Batch 9
# @Group: NinjaWarriors
#-----------------------------------------------------
# Problem Statement:
# To setup the postgres cluster
#
#-----------------------------------------------------
#-------------->>CONFIGURATION FILES<<----------------
#-------------->>ENVIRONMENTAL VARIABLES<<----------------
#-----------------------------------------------------

#set -x

currentSetUp(){
    echo "Please find the current step below"
    sudo pg_lsclusters 
}

createReplicationCluster(){
    echo "Creating a new Replication Cluster"
    sudo pg_createcluster 13 replica  
    sudo pg_ctlcluster 13 replica start
    sudo pg_ctlcluster 13 replica stop
}

createArchivalDirectories(){
    echo "Creating Archival Directory"
    sudo -H -u postgres mkdir /var/lib/postgresql/pg_log_archive/main -p
    sudo -H -u postgres mkdir /var/lib/postgresql/pg_log_archive/replica -p
}

editConfigFilesMainCluster(){
    echo "Adding Revelavnt Setting to config files to the main cluster" 
    echo "# Custom Configuration Block " >> /etc/postgresql/13/main/postgresql.conf 
    echo "wal_level = replica" >> /etc/postgresql/13/main/postgresql.conf 
    echo "wal_log_hints = on" >> /etc/postgresql/13/main/postgresql.conf 
    echo "archive_mode = on" >> /etc/postgresql/13/main/postgresql.conf 
    echo "archive_command = 'test ! -f /var/lib/postgresql/pg_log_archive/main/%f && cp %p /var/lib/postgresql/pg_log_archive/main/%f'" >> /etc/postgresql/13/main/postgresql.conf 
    echo "max_wal_senders = 13" >> /etc/postgresql/13/main/postgresql.conf 
    echo "hot_standby = on" >> /etc/postgresql/13/main/postgresql.conf 
    echo "max_replication_slots = 10" >> /etc/postgresql/13/main/postgresql.conf 

    ## edit host based access file 
    #echo "local replication rep_user trust" >> /etc/postgresql/13/main/pg_hba.conf 
    sed -i '101 i local   replication     rep_user                                trust' /etc/postgresql/13/main/pg_hba.conf
    
}

createReplicationUser(){
    sudo -H -u postgres psql -c "CREATE USER rep_user WITH replication;" 
    sudo systemctl restart postgresql@13-main
}

editConfigFilesReplicaCluster(){
    echo "Adding Revelavnt Setting to config files" 
    echo "# Custom Configuration Block " >> /etc/postgresql/13/replica/postgresql.conf 
    echo "wal_level = replica" >> /etc/postgresql/13/replica/postgresql.conf 
    echo "wal_log_hints = on" >> /etc/postgresql/13/replica/postgresql.conf 
    echo "archive_mode = on" >> /etc/postgresql/13/replica/postgresql.conf 
    echo "archive_command = 'test ! -f /var/lib/postgresql/pg_log_archive/replica/%f && cp %p /var/lib/postgresql/pg_log_archive/main/%f'" >> /etc/postgresql/13/replica/postgresql.conf 
    echo "max_wal_senders = 13" >> /etc/postgresql/13/replica/postgresql.conf 
    echo "hot_standby = on" >> /etc/postgresql/13/replica/postgresql.conf 
    echo "max_replication_slots = 10" >> /etc/postgresql/13/replica/postgresql.conf 

    ## edit host based access file 
    sudo sed -i '101 i local   replication     rep_user                                trust' /etc/postgresql/13/replica/pg_hba.conf
    #echo "local replication rep_user trust" >> /etc/postgresql/13/replica/pg_hba.conf

}

removeInitialReplicaFiles(){
    rm -rf /var/lib/postgresql/13/replica/* 
}

syncClusters(){
    pg_basebackup -D /var/lib/postgresql/13/replica -U rep_user -w -P -R 
    chown -R postgres:postgres /etc/postgresql/13/replica/
    chown -R postgres:postgres /var/lib/postgresql/13/replica/
    sudo pg_ctlcluster 13 replica start 
}

start(){
    currentSetUp
    createReplicationCluster
    createArchivalDirectories
    editConfigFilesMainCluster
    createReplicationUser
    editConfigFilesReplicaCluster
    removeInitialReplicaFiles
    syncClusters
}

#The starting point of the code

start
# sudo pg_dropcluster 13 replica