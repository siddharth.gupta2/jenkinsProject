#!/bin/bash --
#-----------------------------------------------------
# @Date: Mon Mar 29 15:13:12 IST 2021
# @Author: SIDDHARTH GUPTA
# @Batch: Ninja Batch 9
# @Group: NinjaWarriors
#-----------------------------------------------------
# Problem Statement:
# To create a single db back up
#
# Execution: 
#  ./scripname <DB_NAME>
#  ./postgreDbBackup.sh postgres
#
# Output Backup File Location:
# sample output file
# /tmp/Postgress/BackUp_postgres-20210330

databaseName=$1
databaseDir=DB_Backup
date_parameter=$(date '+%Y%m%d')
name=$databaseName-$date_parameter

createTableDump(){
    pg_dump $databaseName > /tmp/Postgress/BackUp/$databaseDir/$name-backup
    echo "Backup Location: /tmp/Postgress/BackUp/$databaseDir"
}

start(){
    if [ -z $databaseName ];
    then
        echo "ERROR: Invalid database name/ DB name not specified"
    else
        echo "BackUp: $databaseName"
        cd /tmp/Postgress/BackUp
        check1=`ls -l DB_Backup> /dev/null 2>&1`
        validation1=`echo $?`
        #echo $validation1
        if [ $validation1 == '0' ];
        then
            createTableDump
        else 
            mkdir $databaseDir
            createTableDump
        fi
    fi
}

#The starting point of the code
start